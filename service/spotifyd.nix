{ config, pkgs, ... }:

let
  cache_path = "${config.users.users.spotifyd.home}/.cache/spotifyd";
  cache_path_files = "${cache_path}/files";
  large_cache_path_files = "/media/data_hdd/.cache/spotifyd/files";
in
{
  services.spotifyd = {
    # don't use SpotifyD service from nixpkgs
    # * PulseAudio seems to not work with DynamicUser
    # * cache's directory is forced
    enable = false;
  };

  # put configuration file on the default location for SpotifyD as per the documentation https://github.com/Spotifyd/spotifyd/blob/cfef0ad92f/README.md#L172
  environment.etc."xdg/spotifyd/spotifyd.conf".text = ''
    [global]
    # Your Spotify account name.
    username = pinage404

    # Your Spotify account password.
    password = ${builtins.readFile ./spotifyd.pass}

    # A command that gets executed and can be used to
    # retrieve your password.
    # The command should return the password on stdout.
    #
    # This is an alternative to the `password` field. Both
    # can't be used simultaneously.
    #password_cmd = command_that_writes_password_to_stdout

    # If set to true, `spotifyd` tries to look up your
    # password in the system's password storage.
    #
    # This is an alternative to the `password` field. Both
    # can't be used simultaneously.
    #use_keyring = true

    # The audio backend used to play the your music. To get
    # a list of possible backends, run `spotifyd --help`.
    #backend = alsa
    backend = pulseaudio

    # The alsa audio device to stream audio to. To get a
    # list of valid devices, run `aplay -L`,
    #device = alsa_audio_device  # omit for macOS

    # The alsa control device. By default this is the same
    # name as the `device` field.
    #control = alsa_audio_device  # omit for macOS

    # The alsa mixer used by `spotifyd`.
    #mixer = PCM

    # The volume controller. Each one behaves different to
    # volume increases. For possible values, run
    # `spotifyd --help`.
    #volume_controller = alsa  # use softvol for macOS

    # A command that gets executed in your shell after each song changes.
    #on_song_change_hook = command_to_run_on_playback_events

    # The name that gets displayed under the connect tab on
    # official clients. Spaces are not allowed!
    #device_name = device_name_in_spotify_connect

    # The audio bitrate. 96, 160 or 320 kbit/s
    #bitrate = 160

    # The directory used to cache audio data. This setting can save
    # a lot of bandwidth when activated, as it will avoid re-downloading
    # audio files when replaying them.
    #
    # Note: The file path does not get expanded. Environment variables and
    # shell placeholders like $HOME or ~ don't work!
    cache_path = ${cache_path}

    # If set to true, audio data does NOT get cached.
    no_audio_cache = false

    # Volume on startup between 0 and 100
    initial_volume = 90

    # If set to true, enables volume normalisation between songs.
    volume_normalisation = true

    # The normalisation pregain that is applied for each song.
    normalisation_pregain = -10

    # The port `spotifyd` uses to announce its service over the network.
    #zeroconf_port = 1234

    # The proxy `spotifyd` will use to connect to spotify.
    #proxy = http://proxy.example.org:8080

    # The displayed device type in Spotify clients.
    # Can be unknown, computer, tablet, smartphone, speaker, tv,
    # avr (Audio/Video Receiver), stb (Set-Top Box), and audiodongle.
    device_type = avr
  '';

  users = {
    users.spotifyd = {
      isSystemUser = true;
      home = "/home/spotifyd";
      createHome = true;
      description = "spotifyd";
      group = "spotifyd";
      shell = pkgs.bash;
      extraGroups = [ "audio" ];
    };
    groups.spotifyd = {};
  };

  systemd.services.spotifyd = {
    wantedBy = [ "multi-user.target" ];
    # cache is stored on HDD to avoid filling SD card
    after = [ "network-online.target" "sound.target" "media-data_hdd.mount" ];
    description = "spotifyd, a Spotify playing daemon";
    serviceConfig = {
      ExecStartPre =
        "${pkgs.coreutils}/bin/mkdir --parents ${large_cache_path_files} ; "
        + "${pkgs.coreutils}/bin/mkdir --parents ${cache_path} ; "
        + "${pkgs.coreutils}/bin/ln --symbolic --force ${large_cache_path_files} ${cache_path_files} ; ";
      ExecStart = "${pkgs.spotifyd}/bin/spotifyd --no-daemon";
      Restart = "always";
      RestartSec = 12;
      DynamicUser = false;
      User = "spotifyd";
      SupplementaryGroups = [ "audio" ];
    };
  };
}
