{
  hardware.pulseaudio = {
    enable = true;

    # there is no real user on the Raspberry Pi
    # running daemon system wide should be safe this allow some services (such as: SpotifyD)
    systemWide = true;

    daemon.config = {
      # USB card is a 5.1 output
      default-sample-channels = 6;
    };
  };
}
