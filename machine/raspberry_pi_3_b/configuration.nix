# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’)

{
  imports = [
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./boot_loader.nix
    ./swapfile.nix
    ./networking.nix
    ./invert_screen_with_the_broken_foot.nix
    ../../hardware/file_system/nix.nix
    ../../hardware/file_system/home.nix
    ../../hardware/file_system/tmp.nix
    ../../hardware/file_system/data_hdd.nix
    ../../hardware/audio.nix
    ./audio_sound_alsa_config.nix
    ./audio_sound_pulseaudio_config.nix
    ../../service/nix_garbage_collector.nix
    ../../service/nix_daemon.nix
    ../../service/auto_upgrade.nix
    ../../service/ssh.nix
    #../../service/apparmor.nix # doesn't have compatible Kernel
    ../../service/firewall.nix
    ../../service/spotifyd.nix
    ../../service/power_management.nix
    # > The package works on aarch64 but uses PIO which uses FHSUserEnv and doesn't work on aarch64.
    # @colemickens:matrix.org
    # https://matrix.to/#/!ACOPNtJclKAfOLQCKK:matrix.org/$159952018644717kBaMj:matrix.org?via=matrix.org&via=kapsi.fi&via=kyju.org
    # ../../service/boinc.nix
    ../../user/pi.nix
    ../../package/admin.nix
    ../../package/cli_tools.nix
    ../../package/fish.nix
    ../../package/nixos.nix
    ../../i18n.nix
    ../../time.nix
    ../../fonts.nix
  ];

  # This value determines the NixOS release with which your system is to be compatible, in order to avoid breaking some software such as database servers
  # You should change this only after NixOS release notes say you should
  system.stateVersion = "20.03";
}
