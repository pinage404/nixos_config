{
  fileSystems."/home" = {
    device = "/dev/disk/by-label/home_nixos";
    fsType = "ext4";
  };
}
