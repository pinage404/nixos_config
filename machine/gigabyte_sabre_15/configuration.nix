# Edit this configuration file to define what should be installed on your system
# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’)

{
  imports = [
    ./hardware_configuration.nix # Include the results of the hardware scan
    ./shutdown_fix.nix
    ./boot_loader.nix
    ../../hardware/file_system/nix.nix
    ../../hardware/file_system/tmp.nix
    ../../hardware/network.nix
    ../../hardware/audio.nix
    ../../hardware/bluetooth.nix
    ../../hardware/mouse.nix
    ../../hardware/keyboard_backlight.nix
    ../../i18n.nix
    ../../time.nix
    ../../hardware/file_system/home.nix
    ../../hardware/file_system/data_hdd.nix
    ../../hardware/file_system/data_ssd.nix
    ../../hardware/file_system/exec_downloaded.nix
    ../../user/pinage404.nix
    ../../user/git.nix
    ../../service/x11.nix
    #    ../../service/lxqt.nix
    ../../service/kde.nix
    ../../package/basic.nix
    ../../package/admin.nix
    ../../package/network.nix
    ../../package/cli_tools.nix
    ../../package/nixos.nix
    ../../package/fish.nix
    ../../service/power_management.nix
    ../../service/docker.nix
    ../../service/ssh.nix
    ../../service/firewall.nix
    ../../service/kdeconnect.nix
    ../../service/virtualbox.nix
    ../../service/apparmor.nix
    ../../service/gpg_agent.nix
    ../../service/printer.nix
    ../../service/flatpak.nix
    ../../fonts.nix
    ../../service/nix_garbage_collector.nix
    ../../service/auto_upgrade.nix
    ../../service/nix_daemon.nix
  ];

  # This value determines the NixOS release with which your system is to be compatible, in order to avoid breaking some software such as database servers
  # You should change this only after NixOS release notes say you should
  system.stateVersion = "20.03";
}
