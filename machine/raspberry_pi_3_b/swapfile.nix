{
  swapDevices = [
    # Use additional swap memory in order to not run out of memory when installing lots of things while running other things at the same time.
    {
      device = "/media/data_hdd/swapfile";
      size = 1024 * 2 * 2 * 2;
      priority = 2;
    }
  ];
}
