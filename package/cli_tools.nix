{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    # Download
    wget
    curl
    rsync

    # File system
    file
    tree
    which
    lsof
    inotify-tools

    # Network
    mtr

    # Archive
    commonsCompress

    # Auto completion
    bash-completion

    # Git / Diff
    diffutils
    wdiff
    gitAndTools.gitFull

    # Security
    gnupg

    # Search
    ripgrep # rg

    # Fuzzy Finder
    fzf

    # Basic command replacement with modern tools
    bat # cat
    exa # ls
    fd # find but newer / faster / better

    # Other
    psmisc
  ];


  programs.bash.enableCompletion = true;
}
