# pinage404's NixOS Configuration

Moved to https://gitlab.com/pinage404/dotfiles

## Installation

```sh
# 1. clone this repo
git clone git@gitlab.com:pinage404/nixos_config.git

# 2. move default folder
sudo mv /etc/nixos /etc/nixos.bak

# 3. make a symlink between this repo and the NixOS configuration folder
sudo ln -sr nixos_config /etc/nixos

# 4. detect and generate hardware configuration
sudo nixos-generate-config

# 5. make a symlink to the configuration for this machine
sudo mv nixos_config/configuration.nix nixos_config/configuration.nix.bak

# 6. use appropriate configuration for the machine
sudo ln -sr nixos_config/machine/gigabyte_sabre_15/configuration.nix nixos_config/configuration.nix
```

[Move `/nix` to another partition](https://nixos.wiki/wiki/Storage_optimization#Moving_the_store)

## Raspberry Pi

Install following the first steps of [this tutorial](https://citizen428.net/blog/installing-nixos-raspberry-pi-3)

### Avoid space issues

Store cache on HDD to avoid filling SD card

```sh
ln -s /media/data_hdd/.cache/ /var/cache
```
