{
  virtualisation.docker = {
    enable = true;
    extraOptions = "-g /media/exec_downloaded/docker";
    liveRestore = false;
  };
}
