{ pkgs, ... }:

{
  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;

  # if you have a Raspberry Pi 2 or 3, pick this:
  # boot.kernelPackages = pkgs.linuxPackages_latest;
  # boot.kernelPackages = pkgs.linuxPackages_4_19;
  # boot.kernelPackages = pkgs.linuxPackages_rpi3;

  # A bunch of boot parameters needed for optimal runtime on RPi 3b+
  # boot.kernelParams = ["cma=256M"];
  boot.loader.raspberryPi.enable = true;
  boot.loader.raspberryPi.version = 3;
  # boot.loader.raspberryPi.uboot.enable = true;
  boot.loader.raspberryPi.firmwareConfig = ''
    # gpu_mem=256

    # this should fix issue where no sound card is detected
    dtparam=audio=on
  '';
  environment.systemPackages = with pkgs; [
    raspberrypi-tools
  ];
}
