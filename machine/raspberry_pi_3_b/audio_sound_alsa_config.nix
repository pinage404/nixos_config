{
  # set USB card as default sound output
  sound.extraConfig = ''
    defaults.pcm.card 1
    defaults.ctl.card 1
  '';
}
