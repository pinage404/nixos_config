# passwd pi

{
  users = {
    groups.pi = {
      gid = 1000;
    };
    users.pi = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/pi";
      createHome = true;
      description = "pi";
      group = "pi";
      extraGroups = [
        "wheel" # to be able to use `sudo` command
        "networkmanager"
        "fuse" # to be able to use FUSE to use AppImage
        "audio"
        "pulse"
      ];
      initialPassword = "pi";
    };
  };
}
