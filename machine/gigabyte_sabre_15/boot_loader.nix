{
  # Use the systemd-boot EFI boot loader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.useOSProber = true; # to add previous installed OS in GRUB

  # Add memtest86
  boot.loader.grub.memtest86.enable = true;
}
