{
  services.xserver = {
    # Enable the X11 windowing system
    enable = true;
    layout = "fr";

    # Enable touchpad support
    libinput.enable = true;

    # Enable C-M-Bksp to kill X
    enableCtrlAltBackspace = true;

    # SDDM (Simple Desktop Display Manager) the default KDE display manager
    displayManager.sddm = {
      enable = true;
      autoNumlock = true;
    };
  };
}
