{
  # stop freezing when upgrading / downloading new packages ...
  nix = {
    daemonNiceLevel = 12;
    daemonIONiceLevel = 3;
  };
}
