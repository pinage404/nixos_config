{
  hardware.bluetooth.enable = true;
  hardware.bluetooth.extraConfig =
    ''
      [General]
      #ControllerMode = bredr
      ControllerMode = dual

      AutoEnable=true

      Disable=Socket
      Disable=Headset
      Enable=Media,Source,Sink,Gateway
      AutoConnect=true
      load-module module-switch-on-connect
    '';
}
