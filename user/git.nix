{
  users = {
    groups.git = {
      gid = 1001;
    };
    users.git = {
      isNormalUser = true;
      uid = 1001;
      home = "/home/git";
      createHome = true;
      group = "git";
      initialPassword = "git";
    };
  };
}
