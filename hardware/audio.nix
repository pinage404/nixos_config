{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    pavucontrol
    paprefs
    ffmpeg
  ];

  # ALSA
  sound.enable = true;
  sound.mediaKeys.enable = true;

  # PulseAudio
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;
}
