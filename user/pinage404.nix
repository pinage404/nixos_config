# passwd pinage404

{
  users = {
    groups.pinage404 = {
      gid = 1000;
    };
    users.pinage404 = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/pinage404";
      createHome = true;
      description = "pinage404";
      group = "pinage404";
      extraGroups = [
        "docker"
        "wheel" # to be able to use `sudo` command
        "networkmanager"
        "vboxusers" # to be able to use USB in VM
        "fuse" # to be able to use FUSE to use AppImage
      ];
      initialPassword = "pinage404";
    };
  };
}
