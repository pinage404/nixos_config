{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    nox
    nix-bash-completions

    # program that help to manage user's programs the same way as /etc/nixos/configuration.nix
    home-manager
  ];
}
