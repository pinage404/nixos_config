{
  # Enable the OpenSSH daemon
  services.openssh = {
    enable = true;
    forwardX11 = true;
  };
}
