{ pkgs, ... }:

{
  programs.mtr.enable = true; # Allow mtr to be used with a non-root user

  # Allow wireshark to be used with a non-root user
  programs.wireshark = {
    enable = true;
    package = pkgs.wireshark;
  };

  environment.systemPackages = with pkgs; [
    networkmanager
    nettools
    iftop
    mtr
  ];
}
