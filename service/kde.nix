{
  services.xserver.desktopManager = {
    # Enable the KDE Desktop Environment
    plasma5.enable = true;
  };
}
