{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    sudo

    # Partitions tools
    gnufdisk
    gparted
    parted
    testdisk
    fuse
    sshfs
    ntfs3g
    dosfstools
    lsof

    # Hardware tools
    usbutils
    powertop
    pmutils
    hddtemp
    lshw
    pciutils
    lm_sensors
    lsb-release
    upower

    # Network
    nettools
    iftop
    mtr

    # Top
    sysstat
    htop
    iftop
    iotop
    powertop
  ];
}
