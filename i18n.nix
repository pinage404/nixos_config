{
  console = {
    font = "fira-code";
    keyMap = "fr";
  };

  i18n = {
    defaultLocale = "fr_FR.UTF-8";
  };
}
