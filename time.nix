{
  # Enable Network Time Protocol
  services.ntp.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Paris";
}
