{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gitAndTools.gitFull
    kate
    chromium
    htop
    networkmanager
  ];
}
